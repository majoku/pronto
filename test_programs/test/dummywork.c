#include <stdio.h>
#include <assert.h>
#include <cblas.h>
#include <omp.h>
//#include <string.h>

void do_work(size_t n, size_t m, double *a, double *b, double *c, int num_threads)
{
    assert(a);
    assert(b);
    assert(c);
    //printf("n: %ld\n", n);
    //printf("m: %ld\n", m);

    const size_t m2 = m*m;
    //memset(c, 0, n*m2 * sizeof(double));
    //
    //printf("openblas parallel: %d\n", openblas_get_parallel());

#pragma omp parallel for num_threads(num_threads)
    for (size_t i = 0; i < n; i++) {
        //printf("i %ld on thread %d\n", i, omp_get_thread_num());
        cblas_dgemm(
                CblasRowMajor, CblasNoTrans, CblasNoTrans, m, m, m,
                1.0, &a[i*m2], m, &b[i*m2], m,
                0.0, &c[i*m2], m);
    }
}

import psutil

for proc in psutil.process_iter():
    print(proc.pid, proc.name(), proc.username(), proc.uids(), proc.exe(), proc.cmdline(), proc.parent(), proc.terminal())

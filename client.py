#!/usr/bin/env python3

import http.client
from job import JobRequest

def run(args):

    job = JobRequest(
            command=args.command,
            name=args.name,
            priority=args.priority,
            time=args.time,
            cores=args.cores)

    def get(conn, url):
        cmd = 'GET'
        conn.request(cmd, url)
        rsp = conn.getresponse()
        return rsp

    def post(conn, url, msg):
        cmd = 'POST'
        conn.request(cmd, url, body=msg)
        rsp = conn.getresponse()
        return rsp

    conn = http.client.HTTPConnection(args.host, port=args.port)

    rsp = post(conn, 'queue_job', job.pickle())
    print('Response from server: {}'.format(rsp))
    print(rsp.read())

    #get response from server
    #print(rsp.status, rsp.reason)
    #data_received = rsp.read()
    #print(data_received)

    conn.close()

def print_queue(self, jobs):
    sep = '   '
    fmtstr = '{:20}{:sep}{:20}{:sep}{:10}'
    print(fmtstr.format('Job name', 'User', 'Status', sep=sep))
    for job in jobs:
        print(fmtstr.format(job.name, job.user.name, job.status))

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('command', type=str, nargs='+')
    parser.add_argument('-n', '--name', type=str, default='unnamed job')
    parser.add_argument('-t', '--time', type=str, default='0')
    parser.add_argument('-c', '--cores', type=int, default=1)
    parser.add_argument('-p', '--priority', type=int, default=0)
    parser.add_argument('--host', type=str, default='127.0.0.1')
    parser.add_argument('--port', type=int, default=8081)
    args = parser.parse_args()

    cmd = []
    for item in args.command:
        cmd.extend(item.split())
    args.command = cmd

    run(args)

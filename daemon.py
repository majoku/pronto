import logging
import time

#logging.basicConfig(filename='info.log', level=logging.INFO)

class Daemon:

    def __init__(self, tick_interval, max_jobs_queued, managed_cores):
        self.tick_interval = tick_interval
        self.max_jobs_queued = max_jobs_queued
        self.managed_cores = managed_cores

        # Starting values
        self.jobs = []
        self.jobs_queued = []
        self.jobs_suspended = []
        self.jobs_running = []
        self.stop = False

    @property
    def n_jobs_queued(self):
        return len(self.jobs_queued)

    @property
    def n_jobs_suspended(self):
        return len(self.jobs_suspended)

    @property
    def n_jobs_running(self):
        return len(self.jobs_running)

    def run_forever(self):
        while(not self.stop):
            # If no jobs are queued or suspended, we have nothing to do!
            if (self.n_jobs_queued > 0 or self.n_jobs_suspended > 0):
                self.remove_completed_jobs()
                self.update_jobs()
            time.sleep(self.tick_interval)
            #break

    def cores(self):
        return self.managed_cores.copy()

    def get_priority_list(self):
        return self.jobs.sort(key=lambda x: x.priority)

    def remove_completed_jobs(self):
        jobs = self.jobs.copy()
        for job in jobs:
            if job.complete:
                #logging.info('Job %s completed.' % job.name)
                self.jobs.remove(job)

    def update_jobs(self):
        cores = self.cores()
        jobs = self.get_priority_list()
        run_jobs = []
        n_free_cores = len(cores)
        while(n_free_cores > 0 and jobs):
            job = jobs.pop()
            run_jobs.append(job)
            n_free_cores -= job.ncores
        # Suspend all other jobs
        for job in jobs:
            if job.status == 'R':
                self.suspend_job(job)
        # Keep running
        active_jobs = [x for x in active_jobs if x.status == 'R']
        inactive_jobs = [x for x in active_jobs if x.status in ('S', 'Q')]
        for job in active_jobs:
            for core in job.cpu_affinity:
                cores.remove(core)
        # Continue and start
        for job in inactive_jobs:
            if job.status == 'S':
                cpu_affinity = cores[:job.ncore]
                del core[:job.ncore]
                self.continue_job(job, cpu_affinity)
            elif job.status == 'Q':
                cpu_affinity = cores[:job.ncore]
                del core[:job.ncore]
                self.start_job(job, cpu_affinity)

    def queue_job(self, job):
        if self.n_jobs_queued >= self.max_jobs_queued:
            #logging.info('Cannot queue jop %s - queue full.' % job.name)
            return False
        else:
            #logging.info('Queueing jop %s.' % job.name)
            self.jobs.append(job)
            return True

    def start_job(self, job, cpu_affinity):
        #logging.info('Starting job %s.' % job.name)
        job.status = 'R'
        job.cpu_affinity = cpu_affinity
        job.start()

    def suspend_job(self, job):
        #logging.info('Suspending job %s.' % job.name)
        job.status = 'S'
        job.cpu_affinity = []

    def continue_job(self, job, cpu_affinity):
        #logging.info('Continueing job %s.' % job.name)
        job.status = 'R'
        job.cpu_affinity = cpu_affinity

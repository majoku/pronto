#!/usr/bin/env python3

import http.server
import threading
import logging
from job import Job, JobRequest
from daemon import Daemon

logging.basicConfig(filename='info.log', level=logging.INFO)

class Handler(http.server.BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header(b'Content-type', b'text/html')
        self.end_headers()

    def do_GET(self):
        print('do_GET')
        self._set_headers()
        self.wfile.write(b'GET MESSAGE FROM SERVER')

    def do_HEAD(self):
        print('do_HEAD')
        self._set_headers()

    def do_POST(self):
        print('do_POST')
        self._set_headers()
        l = int(self.headers.get('content-length', 0))
        if self.path == 'queue_job':
            body = self.rfile.read(l)
            req = JobRequest.unpickle(body)
            print('JobRequest: {}'.format(req))
            job = Job.from_request(req)
            print('Job: {}'.format(job))
            job.run()
            self.wfile.write(b'POST MESSAGE FROM SERVER')
        else:
            self.wfile.write(b'invalid path')

def start_server(host, port):
    logging.info('Starting HTTP server')
    server_address = (host, port)
    server = http.server.HTTPServer(server_address, Handler)
    t = threading.Thread(target=server.serve_forever)
    t.start()
    #logging.info('Starting daemon')
    #daemon = Daemon()
    #logging.info('Stopping daemon')
    #logging.info('Stopping server')
    #t.exit()
    #server.server_close()

def start_daemon(tick_interval, max_jobs_queued, managed_cores):
    logging.info('Starting daemon')
    daemon = Daemon(tick_interval, max_jobs_queued, managed_cores)
    t = threading.Thread(target=daemon.run_forever)
    t.start()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', type=str, default='127.0.0.1')
    parser.add_argument('--port', type=int, default=8081)
    parser.add_argument('--tick-interval', type=float, default=10.0)
    parser.add_argument('--max-jobs-queued', type=int, default=1000)
    parser.add_argument('--managed-cores', type=str, default='2,3')
    args = parser.parse_args()

    core_expr = args.managed_cores.split(',')
    cores = []
    for expr in core_expr:
        if '-' in expr:
            start, end = expr.split('-')
            for core in range(int(start), int(end)+1):
                if core in cores:
                    raise ValueError()
                cores.append(core)
        else:
            core = int(expr)
            if core in cores:
                raise ValueError()
            cores.append(core)
    args.managed_cores = cores

    start_server(host=args.host, port=args.port)
    start_daemon(tick_interval=args.tick_interval, max_jobs_queued=args.max_jobs_queued, managed_cores=args.managed_cores)

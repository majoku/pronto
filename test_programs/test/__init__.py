import sys
import numpy as np
import ctypes as ct
from timeit import default_timer as timer

lib = np.ctypeslib.load_library('libwork', '.')

lib.do_work.argtypes = [
        ct.c_size_t,
        ct.c_size_t,
        np.ctypeslib.ndpointer(dtype=np.float64, flags='A,C'),
        np.ctypeslib.ndpointer(dtype=np.float64, flags='A,C'),
        np.ctypeslib.ndpointer(dtype=np.float64, flags='A,C,W'),
        ct.c_int]
lib.do_work.restype = None

def run():
    n = 2**10
    m = 2**6
    a = np.random.rand(n, m, m)
    b = np.random.rand(n, m, m)
    c = np.zeros((n, m, m))
    nt = int(sys.argv[1])

    #Python
    #t0 = timer()
    #c_py = np.einsum('tij,tjk->tik', a, b)
    #t_py = timer() - t0

    # C
    t0 = timer()
    lib.do_work(n, m, a, b, c, nt)
    t_c = timer() - t0
    print("{} s".format(t_c))

    #print(t_py, t_c)

    #print(np.linalg.norm(c-c_py))
    #assert(np.allclose(c, c_py))

if __name__ == '__main__':
    run()


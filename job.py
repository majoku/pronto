import threading
import pickle
import subprocess
import os

class User:

    def __init__(self):
        self.name = os.getlogin()
        self.gid = os.getgid()
        self.uid = os.getuid()

    def __repr__(self):
        return 'User(name={}, gid={}, uid={})'.format(self.name, self.gid, self.uid)

class JobRequest:

    def __init__(self, command, name, priority, time, cores):
        self.command = command
        self.name = name
        self.priority = priority
        self.time = time
        self.cores = cores
        self.user = User()
        self.cwd = os.getcwd()
        self.env = os.environ.copy()

    def pickle(self):
        return pickle.dumps(self)

    @staticmethod
    def unpickle(string):
        return pickle.loads(string)

    def __repr__(self):
        r = 'JobRequest(command={}, name={}, priority={}, time={}, cores={}, user={}, cwd={})'.format(
                self.command, self.name, self.priority, self.time, self.cores, self.user, self.cwd)
        return r

class Job:

    def __init__(self, user, cwd, env, command, name, priority, time, cores):
        self.user = user
        self.cwd = cwd
        self.env = env
        self.command = command
        self.name = name
        self.priority = priority
        self.time = time
        self.cores = cores

        # Starting values
        self.cpu_affinity = []
        self.status = 'Q'
        self.complete = False
        self.thread = threading.Thread(target=self.run)

    @classmethod
    def from_request(cls, job_request):
        job = cls(
                user=job_request.user, cwd=job_request.cwd, env=job_request.env, command=job_request.command,
                name=job_request.name, priority=job_request.priority, time=job_request.time, cores=job_request.cores)
        return job

    def demote(self):
        os.setgid(self.user.gid)
        os.setuid(self.user.uid)

    def exc(self):
        proc = subprocess.Popen(self.command, preexec_fn=self.demote, cwd=self.cwd, env=self.env)

    def is_running(self):
        return self.thread.is_alive()

    def start(self):
        self.thread.start()

    def run(self):
        self.exc()
        self.complete = True

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, value):
        if value not in ('Q', 'S', 'R', 'E'):
            raise ValueError()
        self._status = value

    def __repr__(self):
        r =  ("Job(command={}, name={}, priority={}, time={}, cores={}, user={}, cpu_affinity={}, "
                "status={}, complete={})").format(
                self.command, self.name, self.priority, self.time, self.cores, self.user,
                self.cpu_affinity, self.status, self.complete)
        return r
